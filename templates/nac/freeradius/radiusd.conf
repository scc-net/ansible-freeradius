prefix = /usr
exec_prefix = /usr
sysconfdir = /etc
localstatedir = /var
sbindir = ${exec_prefix}/sbin
logdir = /var/log/freeradius
raddbdir = /etc/freeradius
radacctdir = ${logdir}/radacct

#  name of the running server.  See also the "-n" command-line option.
name = freeradius

#  Location of config and logfiles.
confdir = ${raddbdir}
modconfdir = ${confdir}/mods-config
certdir = ${confdir}/certs
cadir   = ${confdir}/certs
run_dir = ${localstatedir}/run/${name}

db_dir = ${raddbdir}
libdir = /usr/lib/freeradius
pidfile = ${run_dir}/${name}.pid
correct_escapes = true
max_request_time = 30
cleanup_delay = 5
max_requests = 16384
hostname_lookups = no

log {
	destination = syslog
	colourise = yes
	file = ${logdir}/radius.log
	syslog_facility = daemon
	stripped_names = no
	auth = yes
	auth_badpass = no
	auth_goodpass = no
	msg_goodpass = "OK:%{Stripped-User-Name}|%{Stripped-User-Domain}|%{Tmp-String-3}|%{NAS-Identifier}|%{Tmp-String-2}"
	msg_badpass = "REJECT:%{Stripped-User-Name}|%{Stripped-User-Domain}|%{Tmp-String-3}|%{NAS-Identifier}|%{Tmp-String-2}"
	msg_denied = "You are already logged in - access denied"
}

#  The program to execute to do concurrency checks.
checkrad = ${sbindir}/checkrad

security {
	user = freerad
	group = freerad
	allow_core_dumps = no
	max_attributes = 200
	reject_delay = 1
	status_server = yes
}

proxy_requests  = yes
$INCLUDE proxy.conf

$INCLUDE clients.conf

thread pool {
	start_servers = 5
	max_servers = 32
	min_spare_servers = 3
	max_spare_servers = 10
	max_requests_per_server = 0
	auto_limit_acct = no
}

modules {
	$INCLUDE mods-enabled/
}

instantiate {
}

policy {
	$INCLUDE policy.d/
}

listen {
    type = control
    socket = ${run_dir}/${name}.sock
    mode = rw
}

$INCLUDE sites-enabled/

