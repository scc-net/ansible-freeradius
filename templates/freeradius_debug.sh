#!/usr/bin/env bash

systemctl stop freeradius
mkdir -p /var/run/freeradius
chown freerad:freerad /var/run/freeradius
freeradius -X
